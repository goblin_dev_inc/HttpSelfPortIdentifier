package com.example.HttpSelfPortIdentifier;

import static spark.Spark.get;
import static spark.Spark.port;

public class HTTPHandler {


    HTTPHandler() {
        requestTaker();
    }

    void requestTaker() {
        int port = Integer.parseInt(System.getenv("PORT"));
        port(port);
        get("/", (request, response) -> "Hello! Port is: " + port);
    }
}
